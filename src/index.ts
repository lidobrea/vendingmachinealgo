const express = require('express');
const bodyParser = require('body-parser');

import { Change, CoinBase, LimitedCoinBase } from './coins';
import { LimitedVendomat, Vendomat } from "./vending";

/**
 * Helper to transform a collection of type Change to a stringified, more pleasant human
 * readable format
 * @param {[Change]} change
 * @returns {string}
 */
const changeAmountBeautifyHelper = (change: [Change]): string => {
    return change.reduce((acc: string, change: Change): string => {
        acc += `${change.coins} of ${change.label} `;
        return acc;
    }, '');
};

const vendomat = new Vendomat(CoinBase);
const limitedVendomat = new LimitedVendomat(LimitedCoinBase);

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.set('port', process.env.PORT || 8080);

// Expose vendomat api via REST at localhost:8080/vendomat endpoint.
app.post('/vendomat', (req: express.Request, res: express.Response) => {
    const { pence, limited } = req.body;
    if (limited) {
        res.json(changeAmountBeautifyHelper(limitedVendomat.getChangeFor(pence)));
    } else {
        res.json(changeAmountBeautifyHelper(vendomat.getChangeFor(pence)));
    }
});

app.listen(app.get('port'), () => {
    console.log(`App is running at http://localhost:${app.get(`port`)}`);
});

export { changeAmountBeautifyHelper, vendomat, limitedVendomat };