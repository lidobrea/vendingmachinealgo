const fs = require('fs');
const path = require('path');
const CoinBase = require('../data/coins.json');

export type Change = {
    coins: number;
    label: string;
};

export type Coin = {
    denomination: number,
    label: string,
    count?: number
};

// Create another Coin collection based on the 'coins.json' file
// We will use this one as our base for the limited vending machine usecase.
const LimitedCoinBase: [Coin] = Array.prototype.map.call(CoinBase, (coin: Coin): Coin => {
    coin.count = Math.ceil(Math.random() * 100);
    return coin;
});

// store the above Coin collection dataset into a json file in the /data dir.
fs.writeFileSync(path.join(__dirname, '../data/coin-inventory.json'), JSON.stringify(LimitedCoinBase));

export { CoinBase, LimitedCoinBase };