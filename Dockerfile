# System dependency
FROM node:6.4.0

# Create app runnign directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Move contents to new path
ADD . /usr/src/app/

# Install and build
RUN npm install
RUN npm run build

# Expose app on :8080 and run it
EXPOSE 8080
CMD ["npm", "start"]