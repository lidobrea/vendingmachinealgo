require('../dist/index');

const mocha = require('mocha');
const { expect } = require('chai');

const { CoinBase, LimitedCoinBase } = require('../dist/coins');
const { LimitedVendomat, Vendomat } = require('../dist/vending');

const vendomat = new Vendomat(CoinBase);
const limitedVendomat = new LimitedVendomat(LimitedCoinBase);

describe('Unlimited Coin Vendomat', () => {
    it('Should return a collection of Coin pairs', function (done) {
        const changeCollection = vendomat.getChangeFor(151);
        const change = changeCollection[0];

        expect(changeCollection).to.be.an('array').that.is.not.empty;

        expect(change).to.be.an('object').that.is.not.empty;
        expect(change).to.be.have.property('coins');
        expect(change.coins).to.be.a('number');
        expect(change.coins).to.not.equal(0);

        expect(change).to.be.have.property('label');
        expect(change.label).to.be.a('string');

        done();
    });

    it('Should throw an error stating pence must be of type number', function (done) {
        expect(vendomat.getChangeFor.bind(vendomat, 'asd')).to.throw(/be numeric/);
        done();
    });

    it('Should throw an error stating pence amount is invalid', function (done) {
        expect(vendomat.getChangeFor.bind(vendomat, -25)).to.throw(/invalid/i);
        done();
    });
});

describe('Limited Coin Vendomat', () => {
    it('Should update coinbase', function (done) {
        const changeCollection = limitedVendomat.getChangeFor(1293);
        const modifiedCoinInventory = require('../data/coin-inventory.json');

        expect(LimitedCoinBase).to.not.equal(modifiedCoinInventory);
        done();
    });

    it('Should throw an error stating not enough change', function (done) {
        expect(limitedVendomat.getChangeFor.bind(limitedVendomat, 151432234329432842843284321)).to.throw(/enough change/);
        done();
    });
});