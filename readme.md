# Vending machine algorithm

## [Codepen demo](https://codepen.io/liviudobrea/pen/aEmvrm?editors=0011)

## Description

Quick solution using an OO approach of linear complexity to solving the vending machine optimal change problem. 

## Prerequisites

In order to locally run the typescript version you need to have `typescript` and `ts-node` installed on your machine.
 
For node usage, you only need `node`.

## Install & Building

Just run `npm install` to fetch all dependencies. Then run `npm run build` to transpile the project to ES5 javascript. 

## Running

**Example usage from CLI**

* Unlimited coins
```
node .bin/run.js 150
```
* Limited coins
```
LIMIT=true node .bin/run.js 150
```

**Usage with node** 

`npm start`

**Usage with ts-node**

`ts-node src/index.ts`

Both of the above commands will expose a *POST endpoint* at http://localhost:8080/vendomat.
You can query that via command-line with CURL
 
`curl -H "Content-Type: application/json" -X POST -d '{ "pence": 151 }' 
http://localhost:8080/vendomat`

## Testing

Run `npm run test` to run a bunch of mocha-chai based tests. 

## Docker

In order to use docker you require the `docker daemon` installed and configured locally.

To build a local image run `npm run docker:build`.
To run and expose the image on `8080` run `npm run docker:run`.

### Extras

Run the quick functional implementation `node .bin/quickie.js 125`.