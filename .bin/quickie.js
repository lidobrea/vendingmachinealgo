const pence = process.argv[2];
const { LIMIT } = process.env;

const coins = [[100, 29], [50, 15], [20, 1], [10, 2], [5, 10], [2, 3], [1, 25]];

const getChangeFor = function (limit, sum) {
    return Array.prototype.reduce.call(coins, (acc, coinStack, key) => {
        const value = coinStack[0];
        const coinsLeft = coinStack[1];
        let requiredCoins = Math.floor(sum / value);

        if (limit && requiredCoins > coinsLeft) {
            requiredCoins = coinsLeft;
            coins[key][1] = 0;
        }

        sum -= requiredCoins * value;

        if (key === coins.length - 1 && sum > 0) {
            throw 'Not enough coins';
        }

        if (requiredCoins) {
            acc.push({ denomination: value, coins: requiredCoins });
        }

        return acc;
    }, []);
};

console.log(getChangeFor(LIMIT, pence));