const path = require('path');
const fs = require('fs');

import { Coin, Change } from "./coins";

export class Vendomat {
    constructor(coinReserve: [Coin]) {
        this.reserve = coinReserve;
    }

    protected reserve: [Coin];
    protected trimCoinBase(reserve: [Coin], pence: number): [Coin] {
        return Array.prototype.filter.call(reserve, (coin: Coin): boolean =>
            (pence / coin.denomination > 1 && coin.count !== 0))
    };

    /**
     * Expecting the vending machine to split this into required coin - value pairs.
     * @param {number} pence - The amount to be changed.
     * @returns {[Change]} - Exchanged amount of pence.
     */
    public getChangeFor(pence: number): [Change] {
        if (isNaN(pence)) throw 'Pence must be numeric!';
        if (pence <= 0) throw 'Invalid amount';

        const coinBase: [Coin] = this.trimCoinBase(this.reserve, pence);
        return coinBase.reduce((acc: [Change], coin: Coin): [Change] => {
            const coinsRequired = Math.floor(pence / coin.denomination);
            if (coinsRequired === 0) return acc;
            pence -= coinsRequired * coin.denomination;

            acc.push({ coins: coinsRequired, label: coin.label });
            return acc;
        }, [] as [Change]);
    }
}

export class LimitedVendomat extends Vendomat {
    constructor(coinReserve: [Coin]) {
        super(coinReserve);
    }

    public getChangeFor(pence: number): [Change] {
        if (isNaN(pence)) throw 'Pence must be numeric!';
        if (pence <= 0) throw 'Invalid amount';

        const coinBase: [Coin] = this.trimCoinBase(this.reserve, pence);
        const changeToGive = coinBase.reduce((acc: [Change], coin: Coin, key: number): [Change] => {
            const coinsRequired = Math.floor(pence / coin.denomination);
            if (coinsRequired === 0) return acc;
            if (coin.count < coinsRequired) {
                pence -= coin.count * coin.denomination;
                coin.count = 0;
            } else {
                coin.count -= coinsRequired;
                pence -= coinsRequired * coin.denomination;
            }
            this.reserve[key].count = coin.count;

            if (key === coinBase.length - 1 && pence > 0) {
                throw 'Sorry, the machine does not have enough change for that amount.';
            }

            acc.push({ coins: coinsRequired, label: coin.label });
            return acc;
        }, [] as [Change]);

        // Update the coin inventory after the operation with the remainder of the coins
        fs.writeFileSync(path.join(__dirname, '../data/coin-inventory.json'), JSON.stringify(this.reserve));
        return changeToGive;
    }
}