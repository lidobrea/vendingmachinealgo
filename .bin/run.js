const pence = process.argv[2];
const limit = process.env.LIMIT;
const { CoinBase, LimitedCoinBase } = require('./../dist/coins');
const { Vendomat, LimitedVendomat } = require('../dist/vending');

if (limit) {
    const limitedVendomat = new LimitedVendomat(LimitedCoinBase);
    console.log(limitedVendomat.getChangeFor(pence));
} else {
    const vendomat = new Vendomat(CoinBase);
    console.log(vendomat.getChangeFor(pence));
}